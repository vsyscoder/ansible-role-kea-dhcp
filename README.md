kea-dhcp
=========

Install and configure kea dhcp server

Official site: https://www.isc.org/kea/

Documentation: https://kea.readthedocs.io/en/latest/index.html

Requirements
------------

None

Role Variables
--------------

All variables are described in [defaults/main.yml](defaults/main.yml)

Dependencies
------------

None

Example Playbook
----------------

Playbook example can be found in [molecule/default/converge.yml](molecule/default/converge.yml)

```yaml
---
- name: Converge
  hosts: all
  tasks:
    - name: "Include ansible-role-kea-dhcp"
      include_role:
        name: "ansible-role-kea-dhcp"
```

License
-------

MIT

Author Information
------------------

Aleksey Koloskov
